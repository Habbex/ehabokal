---
title: "How to setup WSL, and how to use it"
date: "2021-03-18"
featuredImage: ../images/sectionImages/robot.jpg
---

This post will present the necessary steps in order to setup Window Subsystem for Linux, and provide some use cases for using WSL in your development environment. You can read more about WSL [here][1] 

## Setup WSL 
To setup WSL on your in Windows machine, follow the [official documentation][2] maintained by Microsoft.

## Install Windows Terminal 

Windows has a [new terminal app][3] that makes it easier to work with command line/shell, and allows you to easily switch between Linux and either command line or PowerShell sessions.  

## Test the setup 
After being done setting up WSL and have installed *Windows Terminal*, run the *Windows Terminal* application and select your installed distro which you installed while setting up WSL. 

When you run *Windows Terminal* , you usually start at your mounted Windows user profile folder  `/mnt/c/Users/{UserName}`.  

In order to go to the root of your WSL distro type `cd`  in *Windows Terminal*.

Let's test our setup by installing a **very important** package🚂😉
1. On your terminal input the following : `sudo apt-get install sl`
2. Enter your password if asked.
3. Run the command  `sl` on your terminal.
4.  Enjoy the result 😄

## Basic Linux commands.

In the previous sections I told you to type `cd` and `sudo apt-get install sl` , but what does these commands mean ?

- Let's start with the first command `cd`, it's the way to navigate within our Linux filesystem.

     - `cd` : Send you to the home folder.
	
     - `cd ..` : Moves you one directory up.
	
     - `cd -` : Moves you to the previous directory. 
		
- Next command we wrote was `sudo apt-get install sl`, this command consist of the following Linux command:
	- `sudo`  : Is short for "SuperUser Do", this command enables you to perform tasks that require administrative or root permissions. However it's not recommended to use this command, unless you are 100% sure what you are doing.
	
	- `apt-get` : Is the packaging system used by [Debian Linux,][4] just like *nuget package management* for .NET or *NPM(node package manager)* for Node.js.
	 	
	- `apt-get install sl` : We pass the option `install` and the name of the package, which in this case is `sl`  

But was does `sl` mean? 🤔  
Let's continue and I'll explain at the end of this section😉

- `ls` : Is used to list the content of the current directory
	-  `ls -R` : Will list all the files in the sub-directories as well.
   
	- `ls -a ` : Will show all the hidden files.
   
	- `ls al` : Will list all the files and directories with detailed information as their current permission, owners, size and etc.
    
-  `mkdir <directoryName>` : Makes a new directory in the file system.

- `touch file.txt` : Creates a new file in the current directory.

- `cat file.txt` : Reads the file. 

- `vim file.txt` : Edit the file with [Vim][5]
		- To **exit** *Vim* editor:
				1. Press **Esc**.
				2. you should see the `--INSERT--` label disappear from the lower left corner.
				3. To save your changes before exiting type `:w` and then type `:q`, or to exit just type `:q`.
        
-  `code file.txt` : Edit the file with [Visual Studio Code CLI][6] 

### So what does `sl` mean then?

We'll nothing basically , it's a fun package that plays with the notion that you sometimes may mistype the listing command `ls` for `sl`  instead😅. Which normally would give you and error, but now give you a 🚂🚋🚋 on your terminal 😄 . 

You can remove the `sl` package by running the following command :
`sudo apt-get remove sl`.  

[1]: https://docs.microsoft.com/en-us/windows/wsl/ "WSL main page"
[2]: https://docs.microsoft.com/en-us/windows/wsl/install-win10 "WSL Docs"
[3]: https://www.microsoft.com/en-us/p/windows-terminal-preview/9n0dx20hk701?activetab=pivot:overviewtab "Windows terminal"
[4]: https://www.debian.org/index.sv.html "Linux Debian"
[5]: https://www.vim.org/ "Vim"
[6]: https://code.visualstudio.com/docs/editor/command-line "Visual Studio Code CLI"
